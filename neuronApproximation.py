import neurolab as nl
import numpy as np


class NeuralNetwork(object):
    def __init__(self, function_choice, x_train, x_out_range, neuron_hidden_layout, epochs_iterations):
        # Create train samples
        x_min = np.min(x_train)
        x_max = np.max(x_train)
        self.x_train = x_train
        self.x = x_out_range
        self.selected_function = function_choice

        # counting target point
        y = self.__selectedFunction(function_choice, x_train)
        self.y_train = y
        # Normalize value
        y, y_normalize_value = self.__dataNormalization(y)

        size = len(x_train)

        input_value = x_train.reshape(size, 1)
        target_value = y.reshape(size, 1)

        # Create network with 2 layers and random initialized
        net = nl.net.newff([[x_min, x_max]], [neuron_hidden_layout, 1])

        # Train network
        net.trainf = nl.train.train_gd
        net.train(input_value, target_value, epochs=epochs_iterations, show=None, goal=0.0001)

        # Simulate network out range
        self.x = x_out_range
        self.y = net.sim(self.x.reshape(self.x.size, 1)).reshape(self.x.size)
        self.y = self.y * y_normalize_value


    @staticmethod
    def __selectedFunction(function_choice, x):
        if function_choice <= 0:
            y = np.sin((x ** 2 - 4) ** 2)
        if function_choice == 1:
            y = np.log(np.absolute(x**7-3*x**3-4*x))
        if function_choice == 2:
            y = 2*np.cos((x+1)**2)
        return y

    @staticmethod
    def __dataNormalization(data):
        if np.absolute(np.min(data)) > np.absolute(np.max(data)):
            normalize_factor = np.absolute(np.min(data))
            data = data / np.absolute(np.min(data))
        else:
            normalize_factor = np.absolute(np.max(data))
            data = data / np.absolute(np.max(data))

        return data, normalize_factor
