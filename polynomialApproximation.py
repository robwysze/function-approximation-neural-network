import numpy as np
import warnings


class PolynomialApproximation:
    def __init__(self, function_choice, x_train, x_out_range, degree):
        warnings.simplefilter('ignore', np.RankWarning)
        self.x = x_out_range
        y = self.__function(function_choice, x_train)
        self.yy = self.__function(function_choice, x_out_range)
        p = np.poly1d(np.polyfit(x_train, y, degree))
        self.p = p(x_out_range)

    @staticmethod
    def __function(function_choice, x):
        if function_choice <= 0:
            y = np.sin((x ** 2 - 4) ** 2)
        if function_choice == 1:
            y = np.log(np.absolute(x ** 7 - 3 * x ** 3 - 4 * x))
        if function_choice == 2:
            y = 2*np.cos((x+1)**2)
        return y
