import wx
import csv
import numpy as np
import diagramPanel as dp
import neuronApproximation as nnApproximation
import polynomialApproximation as polyApproximation
import random


class MainPanel(wx.Frame):
    def __init__(self, parent, window_title="Function Approximation", size=(1200, 700),
                 style=wx.CAPTION | wx.CLOSE_BOX | wx.MINIMIZE_BOX):
        super(MainPanel, self).__init__(parent, title=window_title, size=size, style=style)
        # default input value
        self.defaultIntervalPoints = "15"
        self.defaultHiddenLayout = "5"
        self.defaultIterations = "2000"
        self.defaultDegree = "3"
        self.x_min = -1
        self.x_max = 1
        self.points_out_range = 6

        self.checkInput = False
        self.activated = False

        self.topPanel = wx.Panel(self)
        self.controlPanel = wx.Panel(self.topPanel, -1, pos=(750, 0), size=(450, 700))
        self.controlPanel.SetBackgroundColour("blue")

        # Control panel content
        fontBig = wx.Font(20, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        fontNormal = wx.Font(15, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        topMargin = 32

        infoText = wx.StaticText(self.controlPanel, -1, "Input", (190, 10))
        infoText.SetFont(fontBig)
        # test function
        textFunction = wx.StaticText(self.controlPanel, -1, "Function:", (10, 60))
        textFunction.SetFont(fontNormal)
        basicFunction = ['sin((x^2 - 4)^2)', 'log(|x^7-3x^3-4x|)',
                         '2cos((x+1)^2)']
        self.inputFunction = wx.ComboBox(self.controlPanel, -1, basicFunction[0], size=(160, 30), pos=(260, 58),
                                         choices=basicFunction)
        self.inputFunction.SetFont(fontNormal)
        # points in interval
        textQuantityPoint = wx.StaticText(self.controlPanel, -1, "Number of points in interval:",
                                          (10, 60 + topMargin))
        textQuantityPoint.SetFont(fontNormal)
        self.inputQuantityPoint = wx.TextCtrl(self.controlPanel, -1, self.defaultIntervalPoints, size=(160, 30),
                                              pos=(260, 58 + topMargin), style=wx.TE_RIGHT)
        self.inputQuantityPoint.SetMaxLength(3)
        self.inputQuantityPoint.SetFont(fontNormal)
        # quantity neurons in hidden layout
        textHiddenNeurons = wx.StaticText(self.controlPanel, -1, "Neurons in hidden layout:", (10, 60 + 2 * topMargin))
        textHiddenNeurons.SetFont(fontNormal)
        self.inputHiddenNeuron = wx.TextCtrl(self.controlPanel, -1, self.defaultHiddenLayout, size=(160, 30),
                                             pos=(260, 58 + 2 * topMargin), style=wx.TE_RIGHT)
        self.inputHiddenNeuron.SetMaxLength(3)
        self.inputHiddenNeuron.SetFont(fontNormal)
        # training iterations
        textEpochNumber = wx.StaticText(self.controlPanel, -1, "Number of iterations:", (10, 60 + 3 * topMargin))
        textEpochNumber.SetFont(fontNormal)
        self.inputEpochNumber = wx.TextCtrl(self.controlPanel, -1, self.defaultIterations, size=(160, 30),
                                            pos=(260, 58 + 3 * topMargin), style=wx.TE_RIGHT)
        self.inputEpochNumber.SetMaxLength(5)
        self.inputEpochNumber.SetFont(fontNormal)
        # Polynomial Approximation degree
        textPolynomialDegree = wx.StaticText(self.controlPanel, -1, "Polynomial degree:", (10, 60 + 4 * topMargin))
        textPolynomialDegree.SetFont(fontNormal)
        self.inputPolynomialDegree = wx.TextCtrl(self.controlPanel, -1, self.defaultDegree, size=(160, 30),
                                                 pos=(260, 58 + 4 * topMargin), style=wx.TE_RIGHT)
        self.inputPolynomialDegree.SetMaxLength(2)
        self.inputPolynomialDegree.SetFont(fontNormal)

        # start button
        startButton = wx.Button(self.controlPanel, -1, label='Start', pos=(50, 300), size=(150, 40))
        startButton.SetFont(fontNormal)
        self.startButton = startButton
        self.startButton.Bind(wx.EVT_BUTTON, self.__start)

        # save to file button
        saveButton = wx.Button(self.controlPanel, -1, label='Save', pos=(250, 300), size=(150, 40))
        saveButton.SetFont(fontNormal)
        self.saveButton = saveButton
        self.saveButton.Bind(wx.EVT_BUTTON, self.__saveToFile)
        self.saveButton.Disable()

        # Status bar
        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText("Press START")

        self.Centre()
        self.Show()

    def __start(self, event):
        self.activated = not self.activated

        if self.activated:
            self.startButton.Disable()
            self.saveButton.Disable()

        self.check, input_value = self.__readInput()

        if not self.check:
            self.activated = False
            self.startButton.Enable()
            return

        function_choice = input_value[0]
        points_in_interval = input_value[1]
        neuron_hidden_layout = input_value[2]
        epochs_iterations = input_value[3]
        polynomialDegree = input_value[4]

        x_range = np.linspace(self.x_min, self.x_max, points_in_interval)
        self.x_train_range, self.x_out_range = self.__createRange(x_range)

        self.nnApproximationResult = nnApproximation.NeuralNetwork(function_choice, self.x_train_range,
                                                                   self.x_out_range,
                                                                   neuron_hidden_layout, epochs_iterations)
        self.polyApproximationResult = polyApproximation.PolynomialApproximation(function_choice, self.x_train_range,
                                                                                 self.x_out_range, polynomialDegree)
        self.diagramPanel = dp.DiagramPanel(self.topPanel, x_range,
                                            self.nnApproximationResult, self.polyApproximationResult)

        self.activated = False
        self.startButton.Enable()
        self.saveButton.Enable()

    def __readInput(self):
        function_choice = self.inputFunction.GetCurrentSelection()
        interval_points_string = self.inputQuantityPoint.GetValue()
        hidden_neurons_string = self.inputHiddenNeuron.GetValue()
        epochs_numbers_string = self.inputEpochNumber.GetValue()
        polynomial_degree_string = self.inputPolynomialDegree.GetValue()

        interval_points_is_int = self.__isInt(interval_points_string)
        hidden_neurons_is_int = self.__isInt(hidden_neurons_string)
        epochs_numbers_is_int = self.__isInt(epochs_numbers_string)
        polynomial_degree_is_int = self.__isInt(polynomial_degree_string)

        if not interval_points_is_int or not hidden_neurons_is_int or not epochs_numbers_is_int or not polynomial_degree_is_int:
            self.__errorMessage("Wrong value")
            return False, 0

        interval_points = int(interval_points_string)
        quantity_hidden_neuron = int(hidden_neurons_string)
        epochs_iterations = int(epochs_numbers_string)
        polynomial_degree = int(polynomial_degree_string)

        if interval_points < 5 or quantity_hidden_neuron < 1 or epochs_iterations < 100 or polynomial_degree < 1:
            self.__errorMessage("too small value")
            return False, 0

        if interval_points > 30:
            self.__errorMessage("Number of points too large")
            return False, 0

        if quantity_hidden_neuron > 10:
            self.__errorMessage("Value of hidden neurons is too high")
            return False, 0

        if epochs_iterations > 15000:
            self.__errorMessage("To much iteration")
            return False, 0

        if polynomial_degree >= interval_points or polynomial_degree > 10:
            self.__errorMessage("Value is too high")
            return False, 0

        if interval_points % 2 != 0 and function_choice == 1:
            self.__errorMessage("Wrong value")
            return False, 0

        input_value = [function_choice,
                       interval_points,
                       quantity_hidden_neuron,
                       epochs_iterations,
                       polynomial_degree
                       ]

        self.saveButton.Enable()

        return True, input_value

    def __isInt(self, string):
        try:
            int(string)
            return True
        except ValueError:
            return False

    def __errorMessage(self, message):
        dlg = wx.MessageDialog(None, message, "Error", wx.OK | wx.ICON_WARNING)
        dlg.ShowModal()
        dlg.Destroy()
        self.inputQuantityPoint.SetValue(self.defaultIntervalPoints)
        self.inputHiddenNeuron.SetValue(self.defaultHiddenLayout)
        self.inputEpochNumber.SetValue(self.defaultIterations)
        self.inputPolynomialDegree.SetValue(self.defaultDegree)
        return False, 0

    def __saveToFile(self, event):
        saveFileDialog = wx.FileDialog(self, "Save As", "", "",
                                       "CSV files (*.csv)|*.csv",
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        saveFileDialog.ShowModal()
        path = saveFileDialog.GetPath()
        saveFileDialog.Destroy()
        x = list(self.x_out_range)
        y = list(self.polyApproximationResult.yy)
        x_delete = list(self.diagramPanel.x_delete)
        y_delete = list(self.diagramPanel.y_delete)
        yNN = list(self.nnApproximationResult.y)
        yPA = list(self.polyApproximationResult.p)

        with open(path, 'w', newline='') as csvFile:
            fieldnames = ['x', 'f(x)', 'yNN(x)', 'yPA(x)', '', 'x deleted', 'f(x) deleted']
            writer = csv.DictWriter(csvFile, fieldnames=fieldnames)
            writer.writeheader()
            for i in range(len(x)):
                if i < np.size(x_delete):
                    writer.writerow({'x': x[i], 'f(x)': y[i], 'yNN(x)': yNN[i], 'yPA(x)': yPA[i],
                                     'x deleted': x_delete[i], 'f(x) deleted': y_delete[i]})
                else:
                    writer.writerow({'x': x[i], 'f(x)': y[i], 'yNN(x)': yNN[i], 'yPA(x)': yPA[i]})

    def __createRange(self, x_range):
        x_tmp = np.linspace(1.0, 1.5, 5)
        x_out_range = []

        for i in range(2):
            for j in range(np.size(x_tmp)):
                if x_tmp[j] != 1 and x_tmp[j] != -1:
                    x_out_range = np.append(x_out_range, x_tmp[j])
            x_tmp = x_tmp * -1

        x_out_range = np.append(x_out_range, x_range)
        x_out_range = np.sort(x_out_range)
        range_size = np.size(x_range)
        random_quantity = round(range_size * 0.15)
        to_delete_value = random.sample(range(range_size), int(random_quantity))

        x_train_range = []
        for i in range(np.size(x_range)):
            check = False
            for j in range(random_quantity):
                if i == to_delete_value[j]:
                    check = True
            if not check:
                x_train_range = np.append(x_train_range, x_range[i])

        return x_train_range, x_out_range
