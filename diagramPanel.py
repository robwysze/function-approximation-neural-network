import wx
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas


class DiagramPanel(wx.Panel):
    def __init__(self, parent, inp, nnApproximationResult, polyApproximationResult):
        wx.Panel.__init__(self, parent, -1, pos=(0, 0), size=(750, 700))
        # value of the selected function
        threshold = 100000
        x = np.linspace(-1.4, 1.4, threshold)
        self.x_delete = self.__deleteValues(inp, nnApproximationResult.x_train)

        y = self.__function(nnApproximationResult.selected_function, x)
        target = self.__function(nnApproximationResult.selected_function, inp)
        self.y_delete = self.__function(nnApproximationResult.selected_function, self.x_delete)

        self.figure = Figure()
        self.figure.set_size_inches(7.5, 6.5)

        # show result
        self.axes = self.figure.add_subplot(111)
        self.axes.axhline(linewidth=1.2, color="black")
        self.axes.axvline(linewidth=1.2, color="black")

        self.axes.set_xlabel('X')
        self.axes.set_ylabel('f(x)')
        self.axes.grid()

        legend1, legend2, legend3, legend4, legend5 = self.axes.plot(x, y, 'b-',
                                                                     inp, target, 'bs',
                                                                     self.x_delete, self.y_delete, 'rs',
                                                                     nnApproximationResult.x, nnApproximationResult.y,
                                                                     'gh',
                                                                     polyApproximationResult.x,
                                                                     polyApproximationResult.p, 'yp'
                                                                     )

        self.figure.legend((legend1, legend2, legend3, legend4, legend5), ('f(x)', 'target value',
                                                                           'deleted target value',
                                                                           'nn approximation result',
                                                                           'polynomial approximation result'),
                           'upper center', ncol=3)
        self.canvas = FigureCanvas(self, -1, self.figure)

    @staticmethod
    def __function(function_choice, x):
        if function_choice <= 0:
            y = np.sin((x ** 2 - 4) ** 2)
        if function_choice == 1:
            y = np.log(np.absolute(x ** 7 - 3 * x ** 3 - 4 * x))
        if function_choice == 2:
            y = 2*np.cos((x+1)**2)

        return y

    def __deleteValues(self, x_range, x_train):
        delete_x = []
        for i in range(np.size(x_range)):
            counter = 0
            for j in range(np.size(x_train)):
                if x_range[i] == x_train[j]:
                    counter += 1
            if counter == 0:
                delete_x = np.append(delete_x, x_range[i])

        return delete_x
